<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="body">
	<div class="container-fluid">
		<div class="row nav_bar">
			<div class="col-md-12 col-xs-10">
				<nav class="navbar navbar-default pull pull-right nav_bb">
					<div class="div_nav">
						<button type="button" class=" navbar-toggle text-center" id="but1" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<ul class="nav navbar-nav collapse navbar-collapse">
							<li class="btn_li"><a href="pages/inscription.php" class="btn btn-warning btn_a">INSCRIPTION</a></li>
							<li class="btn_li"><a href="pages/connexion.php"  class="btn btn-warning btn_a">CONNEXION</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</body>
	
</html>