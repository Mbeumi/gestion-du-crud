<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Header</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style1.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
</head>
<body class="corps">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-7 col-md-5">
				<nav class="navbar navbar-default" style="background-color: inherit !important; border:none !important;">
					<div class="div_nav">
						<button type="button" class=" navbar-toggle text-center" id="but1" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<ul class="nav navbar-nav collapse navbar-collapse pull pull-right">
							<li><a href="pages/inscription.php" class="btn btn-warning">INSCRIPTION</a></li>
							<li><a href="pages/connection.php" class="btn btn-warning">CONNEXION</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>	
		
	</div>


</body>
</html>